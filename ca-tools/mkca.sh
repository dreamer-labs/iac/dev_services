#!/bin/bash

mkdir -p root-ca/{certreqs,certs,crl,newcerts,private}

cd root-ca/

chmod 700 private
touch root-ca.index
echo 00 > root-ca.crlnum
openssl rand -hex 16 > root-ca.serial
#curl -o https://roll.urown.net/_downloads/1daede3e61516d4ccdba1eaf26bdb86c/root-ca.cnf
export OPENSSL_CONF=./root-ca.cnf

mkdir -p intermediate-ca/{certreqs,certs,crl,newcerts,private}
cd root-ca/

chmod 700 private
touch intermed-ca.index
echo 00 > intermed-ca.crlnum
openssl rand -hex 16 > intermed-ca.serial
#curl -o https://roll.urown.net/_downloads/7514c4fc42236f15a393223f7d7c98d9/intermed-ca.cnf
export OPENSSL_CONF=./intermed-ca.cnf
