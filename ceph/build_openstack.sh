#!/usr/bin/env bash

BASE_NAME=${BASE_NAME:-ceph-dev}
IMAGE_ID=${IMAGE_ID:-2429d1c9-ff5f-4943-a663-38170191b1b2}
EX_NET_ID=${EX_NET_ID:-6c928965-47ea-463f-acc8-6d4a152e9745}
IN_NET_ID=${IN_NET_ID:-8ba4f9aa-032a-4a33-83f5-d9bea5a03218}
SSH_KEY=${SSH_KEY:-gitlab}
FLAVOR=${FLAVOR:-s1-4}
VOLUME_SIZE=${VOLUME_SIZE:-50}

echo "[ceph]" > inventory.ini
echo "# hosts entries for ceph nodes" > hosts

declare -a PUBLIC_IPS=()
declare -a PRIVATE_IPS=()
declare -a HOSTNAMES=()
declare -a CEPH_NODES=()

for i in 1 2 3 ; do
  SERVER_NAME=${BASE_NAME}-0${i}
  echo "Creating ${SERVER_NAME}..."
  VOL_ID=$(openstack volume create -f value -c id --size ${VOLUME_SIZE} --type classic ${BASE_NAME}-vol${i})
  INSTANCE_ID=$(openstack server create -f value -c id --wait \
                          --nic net-id=${EX_NET_ID} \
                          --nic net-id=${IN_NET_ID} \
                          --flavor ${FLAVOR} \
                          --image ${IMAGE_ID} \
                          --key-name ${SSH_KEY} \
                          --block-device-mapping sdb=${VOL_ID} \
                          ${SERVER_NAME})
  OS_ADDRESSES=$(openstack server show ${INSTANCE_ID} -f value -c addresses)
  PUBLIC_IP=$(echo $OS_ADDRESSES | cut -d';' -f1 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}")
  PRIVATE_IP=$(echo $OS_ADDRESSES | cut -f 3 -d '=')
  PUBLIC_IPS+=("${PUBLIC_IP}")
  PRIVATE_IPS+=("${PRIVATE_IP}")
  HOSTNAMES+=("${SERVER_NAME}")
  CEPH_NODES+=("${SERVER_NAME}:${PUBLIC_IP}")
done

# Create an ansible inventory file with our new servers.
cat > inventory.ini <<EOL
[ceph]
$(printf "%s\n" "${PUBLIC_IPS[@]}")

[ceph:vars]
ansible_connection=ssh
ansible_ssh_user=centos
ansible_ssh_common_args='-o StrictHostKeyChecking=no'
EOL

# Create a hosts file
cat > hosts <<EOL
$(paste <(printf "%s\n" "${PUBLIC_IPS[@]}") <(printf "%s\n" "${HOSTNAMES[@]}"))
EOL

sleep 30

# The ansible playbook will flush iptables and prepare the nodes for ceph.
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i inventory.ini configure_nodes.yml

ceph-deploy --username centos new ${CEPH_NODES[@]}
ceph-deploy --username centos install --stable nautilus ${PUBLIC_IPS[@]}
ceph-deploy --username centos mon create ${CEPH_NODES[@]}
ceph-deploy --username centos gatherkeys ${PUBLIC_IPS[@]}
for ip in ${PUBLIC_IPS[@]} ; do
  ceph-deploy --username centos osd create \${ip} --data /dev/sdb
done
ceph-deploy --username centos mds create ${PUBLIC_IPS[@]}
ceph-deploy --username centos admin ${PUBLIC_IPS[@]}
ceph-deploy --username centos forgetkeys