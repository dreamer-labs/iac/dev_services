
# functions
pathadd() {
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]];
    then
        if [ "$2" == "pre" ];
        then
            PATH="$1:$PATH";
        else
            PATH="${PATH:+"$PATH:"}$1";
        fi
    fi
}

# Override exit to prompt. This will help us write cleaner documentation.
exit() {
  read -p "Are you sure you want to exit the nawt container? (y|n): " -n 1 -r
  echo
  if [[ ! $REPLY =~ ^[Yy]$ ]]
  then
    echo "okay"
  else
    unset -f exit
    exit
  fi
}

nawt() {
  echo "You are already inside the nawt container. Proceed."
}

# Make vault tooling available

chmod u+x /dev_services/vault/scripts/*.sh
pathadd /dev_services/vault/scripts

alias ls="ls --color"
alias ll="ls -l"

which vault >/dev/null && complete -C $(which vault) vault

[[ -f /dev_services/.bashrc ]] && source /dev_services/.bashrc
