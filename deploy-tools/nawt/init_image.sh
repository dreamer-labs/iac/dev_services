#!/bin/bash

[[ -f /bin/vault ]] || ln -s /tmp/vault_bin/vault /bin/vault;

[[ -f /dev_services/deploy-tools/nawt/.bashrc ]] && cp /dev_services/deploy-tools/nawt/.bashrc /root/.bashrc

exec "$@"
