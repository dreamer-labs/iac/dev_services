# Set up some default variables so we can simply source this file...
RED='\033[0;31m'
YEL='\033[0;33m'
NC='\033[0m' # No Color

export PAAS_ENV=${HOME}/.paas
mkdir -p ${PAAS_ENV}

# This is a terrible way to do this. We should rename every variable to start
# with PAAS_ so that it's easier to grep for the variables, but that would
# require a major documentation and script re-write that we don't have time for.

VAR_LIST='ENVIRONMENT|RANCHER_FQDN|RANCHER_IP|REGISTRY_HOST_FQDN|TLS_TTL|PUBLIC_INTERFACE|SVC_HOME|REGISTRY_HOST_IP|VAULT_ARCHIVE|ROOT_CA|KUBECONFIG'
# The following FQDNs may need to be updated for your environment!
export ENVIRONMENT=${ENVIRONMENT:-staging}
export RANCHER_FQDN=${RANCHER_FQDN:-rancher.devservices.int}
export RANCHER_IP=${RANCHER_IP:-10.20.5.105}
export REGISTRY_HOST_FQDN=${REGISTRY_HOST_FQDN:-registry.devservices.int}
export TLS_TTL=${TLS_TTL:-8760h}
export PUBLIC_INTERFACE=${PUBLIC_INTERFACE:-$(route -n | awk '$1 == "0.0.0.0" {print $8}')}
export SVC_HOME=${SVC_HOME:-$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )}
export REGISTRY_HOST_IP=${REGISTRY_HOST_IP:-$(ip -4 addr show $PUBLIC_INTERFACE | grep -oP '(?<=inet\s)\d+(\.\d+){3}')}
export VAULT_ARCHIVE=${VAULT_ARCHIVE:-$(ls $SVC_HOME/*.tar.gz | sort -n -t _ -k 2 | tail -1)}

export ROOT_CA=${ROOT_CA:-$SVC_HOME/ca-tools/demo-ca/root-ca.crt}

echo -e "=== ENVIRONMENT VARIABLES FOR PAAS INSTALL ==="
echo
grep -qF "${REGISTRY_HOST_FQDN}" /etc/hosts || echo "$REGISTRY_HOST_IP $REGISTRY_HOST_FQDN" | sudo tee -a /etc/hosts

echo -e "Using ENVIRONMENT:        $ENVIRONMENT"
echo -e "Using SVC_HOME:           $SVC_HOME"
echo -e "Using RANCHER_FQDN:       $RANCHER_FQDN"
echo -e "Using RANCHER_IP:         $RANCHER_IP"
echo -e "Using REGISTRY_HOST_FQDN: $REGISTRY_HOST_FQDN"
echo -e "Using PUBLIC_INTERFACE:   $PUBLIC_INTERFACE"
echo -e "Using REGISTRY_HOST_IP:   $REGISTRY_HOST_IP"
echo -e "Using VAULT_ARCHIVE:      $VAULT_ARCHIVE"
echo -e "Using TLS_TTL:            $TLS_TTL"
echo -e "Using ROOT_CA:            $ROOT_CA"
echo -e ""
echo -e "${RED}Please make note of the output above and if anything looks${NC}"
echo -e "${RED}incorrect, manually set the appropriate variables using 'export'${NC}"
echo -e "${RED}!!!!!     AND THEN RE-SOURCE THIS SCRIPT     !!!!!${NC}"
echo
echo -e "Writing ${PAAS_ENV}/env with our environment variables."
printenv | grep -E -- ${VAR_LIST} > ${PAAS_ENV}/env
sed -i -e 's/^/export /' ${PAAS_ENV}/env
if [[ -f "${PAAS_ENV}/vault-token" ]]; then
    cat "${PAAS_ENV}/vault-token" >> ${PAAS_ENV}/env
fi
echo
echo -e "Ensuring that $HOME/.bashrc is set to reload our environment on login."
grep -qF ".paas" $HOME/.bashrc || echo ". ${PAAS_ENV}/env" >> ${HOME}/.bashrc
echo
echo -e "Updating /etc/hosts/ with the docker registry fqdn. Requires sudo."
grep -qF "${REGISTRY_HOST_FQDN}" /etc/hosts || echo "$REGISTRY_HOST_IP $REGISTRY_HOST_FQDN" | sudo tee -a /etc/hosts
echo
echo -e "You may now use the '${YEL}nawt${NC}' command to enter your docker container."

nawt() {
  cd ${SVC_HOME}/deploy-tools
  KUBECONFIG=/dev_services/${KUBECONFIG//$SVC_HOME} ROOT_CA=/dev_services/${ROOT_CA//$SVC_HOME} SVC_HOME=/dev_services docker-compose run \
        -v $SVC_HOME:/dev_services \
        -v $HOME/.ssh/:/root/.ssh \
        -v $HOME/.paas:/root/.paas \
        -e REGISTRY_HOST_IP \
        -e REGISTRY_HOST_FQDN \
        -e RANCHER_FQDN \
        -e RANCHER_IP \
        -e VAULT_TOKEN \
        -e ROOT_CA \
        -e SVC_HOME \
        -e TLS_TTL \
	-e KUBECONFIG \
         nawt /bin/bash
  # Writing files inside the container sets them to mode :root. I think we could set them
  # to :1000 but this will work for now...
  if [[ -f "${PAAS_ENV}/vault-token" ]]; then
    grep -qF "${PAAS_ENV}/vault-token" ${PAAS_ENV}/env || cat "${PAAS_ENV}/vault-token" >> ${PAAS_ENV}/env
  fi
  . ${PAAS_ENV}/env
  cd -
}

exit() {
  read -p "Are you sure you want to exit ${HOSTNAME}? (y|n): " -n 1 -r
  echo
  if [[ ! $REPLY =~ ^[Yy]$ ]]
  then
    echo "okay"
  else
    unset -f exit
    exit
  fi
}
