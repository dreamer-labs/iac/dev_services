# Global variables
clusterDomain: cluster.local
imagePullPolicy: IfNotPresent

expose:
  # Set the way how to expose the service. Set the type as "ingress","clusterIP","loadBalancer"
  # or "nodePort" and fill the information in the corresponding
  # section
  type: loadBalancer
  tls:
    enabled: true
    # Fill the name of secret if you want to use your own TLS certificate
    # and private key. The secret must contain keys named tls.crt and
    # tls.key that contain the certificate and private key to use for TLS
    # The certificate and private key will be generated automatically if
    # it is not set
    secretName: "gitlab-ssl-certs"

  loadBalancer:
    # The name of LoadBalancer service
    # name: gitlab
    # Set the IP if the LoadBalancer supports assigning IP
    IP: "10.20.5.160"
    ports:
      # The service port Gitlab listens on when serving with SSH
      ssh: 22
      # The service port Gitlab listens on when serving with HTTP
      http: ""
      https: 443

# The persistence is enabled by default and a default StorageClass
# is needed in the k8s cluster to provision volumes dynamicly.
# Specify another StorageClass in the "storageClass" or set "existingClaim"
# if you have already existing persistent volumes to use
persistence:
  enabled: true
  # Setting it to "keep" to avoid removing PVCs during a helm delete
  # operation. Leaving it empty will delete PVCs after the chart deleted
  resourcePolicy: ""
  persistentVolumeClaim:
    git_data:
      # Use the existing PVC which must be created manually before bound
      existingClaim: ""
      # Specify the "storageClass" used to provision the volume. Or the default
      # StorageClass will be used(the default).
      # Set it to "-" to disable dynamic provisioning
      storageClass: "csi-cephfs-sc"
      subPath: ""
      accessMode: ReadWriteMany
      size: 20Gi
    etc:
      existingClaim: ""
      storageClass: "csi-cephfs-sc"
      subPath: ""
      accessMode: ReadWriteMany
      size: 5Gi
    backup:
      existingClaim: ""
      storageClass: "csi-cephfs-sc"
      subPath: ""
      accessMode: ReadWriteMany
      size: 100Gi
    ci_builds:
      existingClaim: ""
      storageClass: "csi-cephfs-sc"
      subPath: ""
      accessMode: ReadWriteMany
      size: 100Gi
    rails_shared:
      existingClaim: ""
      storageClass: "csi-cephfs-sc"
      subPath: ""
      accessMode: ReadWriteMany
      size: 100Gi
    rails_uploads:
      existingClaim: ""
      storageClass: "csi-cephfs-sc"
      subPath: ""
      accessMode: ReadWriteMany
      size: 100Gi

# redis:
#   image:
#     registry: registry.devservices.int
#     repository: bitnami/redis
#     tag: 6.0.3-debian-10-r2
#   redisPort: 6379
#   master:
#     persistence:
#       storageClass: "csi-cephfs-sc"
#   slave:
#     persistence:
#       storageClass: "csi-cephfs-sc"

core:
  ssh_host_keys:
    enabled: false
    secretName: ssh-host-keys
  trusted_certs:
    enabled: false
    secretName: ssl-trusted-certs
  busybox:
    registry: registry.devservices.int
    repository: busybox
    tag: 1.31.1-glibc
  image:
    registry: registry.devservices.int
    repository: gitlab/gitlab-ce
    tag: 12.10.6-ce.0
  replicas: 1
  ## Additional deployment annotations
  podAnnotations: {}
  # resources:
  #  requests:
  #    memory: 256Mi
  #    cpu: 100m
  nodeSelector: {}
  tolerations: []
  affinity: soft
  database_info_chart: "postgresql-ha"
  env:
    EXTERNAL_URL: gitlab.example.com
    GITLAB_BACKUP_SCHEDULE: "daily"
    GITLAB_BACKUP_TIME: 12:00
    GITLAB_REDIS_EXTERNAL: false
    # GITLAB_DATABASE_HOST: ""
    # GITLAB_DATABASE_USERNAME: ""

backup:
  image:
    repository: registry.devservices.int/bitnami/kubectl
    tag: 1.17.4

postgresql-ha:
  clusterDomain: "cluster.local"
  postgresqlImage:
    registry: registry.devservices.int
    repository: bitnami/postgresql-repmgr
    tag: 11.8.0-debian-10-r2
    pullPolicy: IfNotPresent
    debug: true
  pgpoolImage:
    registry: registry.devservices.int
    repository: bitnami/pgpool
    tag: 4.1.1-debian-10-r93
    pullPolicy: IfNotPresent
    debug: true
  volumePermissionsImage:
    registry: registry.devservices.int
    repository: bitnami/minideb
    tag: buster
    pullPolicy: Always
  metricsImage:
    registry: registry.devservices.int
    repository: bitnami/postgres-exporter
    tag: 0.8.0-debian-10-r114
    pullPolicy: IfNotPresent
    debug: false
  postgresql:
    replicaCount: 3
    username: postgres
    existingSecret: postgres-secrets
    updateStrategyType: RollingUpdate
    affinity:
      podAntiAffinity:
        preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                  - key: "app"
                    operator: In
                    values:
                    - postgresql-ha
              topologyKey: "kubernetes.io/hostname"
    pdb:
      create: true
      minAvailable: 1
    initdbScripts:
      setup_gitlab.sql: |
        CREATE EXTENSION pg_trgm;
        CREATE database gitlabhq_production;
    pgpool:
      replicaCount: 1
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 100
              podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: "app"
                      operator: In
                      values:
                      - postgresql-ha
                topologyKey: "kubernetes.io/hostname"
      pdb:
        create: true
        minAvailable: 1
    metrics:
      enabled: true
    networkPolicy:
      enabled: false
  persistence:
    enabled: true
    storageClass: "csi-cephfs-sc"
    accessModes:
      - ReadWriteOnce
    size: 20Gi
