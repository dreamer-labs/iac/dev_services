#!/bin/bash
# This script will not create private projects for some reason. We may have to do this manually
user=$1
pass=$2
echo "${pass}"

for project in $(cat harbor_api_projects.txt | jq -r '.[] | .name + "," + (.public|tostring)'); do
	echo "Echoing project $(echo $project | cut -d, -f1)"
	echo " with permissions $(echo $project | cut -d ',' -f2)"
        curl --request POST \
         	-u ${user}:${pass} \
         	--url https://harbor.prod.badger.net/api/projects \
         	--header 'content-type: application/json' \
	 	--data '{"project_name": "'$(echo $project|cut -d, -f1)'", "metadata": {"public": "'$(echo $project|cut -d, -f2)'"}}' -k
done
