#!/usr/bin/env bash

user=$1
pass=$2
echo "${pass}"

for i in $(curl -s -u ${user}:${pass} -k -X GET "https://harbor.prod.badger.net/api/projects" | jq .[].project_id); do

done

curl -s -u ${user}:${pass} -k -X GET "https://harbor.prod.badger.net/api/v4/projects" | sed '1d;$d' | cut -d'"' -f 2
done
