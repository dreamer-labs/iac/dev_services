#!/usr/bin/env bash

user=$1
pass=$2

projects=$(curl -s -u ${user}:${pass} -k -X GET "https://harbor.badger.net/api/projects")

for i in $(echo $projects | jq '.[].project_id') ; do
  curl -s -u ${user}:${pass} -k -X GET "https://harbor.badger.net/api/repositories?project_id=${i}" | sed '1d;$d' | cut -d'"' -f 2 >> harbor_api_images.txt
done
