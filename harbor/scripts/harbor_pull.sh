#!/bin/bash

OLD_HARBOR_FQDN="docker.io"
NEW_HARBOR_FQDN="harbor.prod.badger.net"

# Remember to `docker login`

for image in $(cat harbor_api_images.txt); do
  for tag in $(curl -s -k "https://harbor.badger.net/api/repositories/$image/tags" | jq -r '.[]'); do
    echo "Pulling image $image:$tag"
	  docker pull "$OLD_HARBOR_FQDN/$image:$tag" && \
	  docker tag "$OLD_HARBOR_FQDN/$image:$tag" "$NEW_HARBOR_FQDN/$image:$tag" && \
    docker push "$NEW_HARBOR_FQDN/$image:$tag"
  done
done
