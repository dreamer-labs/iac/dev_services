#!/usr/bin/env bash

function info(){
  blue="\033[0;34m"
  NC='\033[0m'
  echo -e "${blue}${1}${NC}"
}

function heading(){
  echo -e "\n## ${1} ##\n"
}

function default_or_user(){
  # $1: VARIABLE NAME
  # $2: Hint for prompt value
  # $3: Default value defaults to ''
  if [[ -z "${!1}" ]]; then
    if [[ -n "${2}" ]]; then
      hint="(ex: ${2})"
    fi

    if [[ -n "${3}" ]]; then
      def="[${3}]"
    else
      def="[]"
    fi
    cr=`echo $'\n.'`
    cr=${cr%.}
    yellow=`echo $'\e[0;33m.'`
    yellow=${yellow%.}
    nc=`echo $'\e[0m.'`
    nc=${nc%.}
    read -p "${yellow}Enter ${1}$cr      ${hint} $def: ${nc}" from_user
  fi

  if [[ -z "${from_user}" ]] && [[ -n "${3}" ]]; then
    from_user="${3}"
  fi

  echo -n "${from_user}"
}

if [[ -f configure.sh.cfg ]]; then
  source configure.sh.cfg
fi

usage="$(basename "$0") [-h] filename.yml

positional arguments:
    deployment_name name of configuration file to output
options:
    -h  show this help text"

while getopts ':h' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done
shift $((OPTIND - 1))

heading "Deployment Name"

[[ -z "${ROOT_CA}" ]] && echo "ROOT_CA is not set Exiting..." && exit 1;

DEPLOYMENT_NAME=$(default_or_user "DEPLOYMENT_NAME" "cluster_DEV" "${1}")

OUTFILE="${DEPLOYMENT_NAME}.yml"

declare -a HOSTNAMES=()
declare -a PUBLIC_IPS=()
declare -a PRIVATE_IPS=()
declare -a HOST_BLOCKS=()

# Collect hostnames and IPs of kubernetes nodes from the user.
heading "Define Kubernetes nodes"

kube_nodes_answers="${DEPLOYMENT_NAME}_kube_nodes"
if [[ -n "${!kube_nodes_answers}" ]]; then
  answer=$(default_or_user "answer" "Found node definitions in $kube_nodes_answers would you like to use those? [y]/n: ")
  if [[ "$answer" == 'y' ]] || [[ -z "$answer" ]]; then
    while read line; do
      HOSTNAMES+=("$(echo "$line" | cut -f1 -d' ' )")
      PUBLIC_IPS+=("$(echo $line | cut -f2 -d' ')")
      PRIVATE_IPS+=("$(echo $line | cut -f3 -d' ' )")
    done <<<"${!kube_nodes_answers}"
  fi
else
  while true; do
    info "Enter infos for to a new kubernetes node"
    _hostname=$(default_or_user "hostname" "new kubernetes node or Enter if finished")
    [[ -z $_hostname ]] && break
    _public_ip=$(default_or_user "public_ip" "new kubernetes node public ip")
    _private_ip=$(default_or_user "private_ip" "new kubernetes node private_ip" "${_public_ip}")

    HOSTNAMES+=("${_hostname}")
    PUBLIC_IPS+=("${_public_ip}")
    PRIVATE_IPS+=("${_private_ip}")
    info "Saved kubernetes node ${_hostname} - pub:${_public_ip} - priv:${_private_ip}"
    unset _public_ip
    unset _private_ip
    unset _hostname
  done
fi

info "Nodes defined for this environment: ${HOSTNAMES[*]}"

# Collect DNS DNS_Resolvers
if [[ -z "${DNS_RESOLVERS}" ]];
then
  DNS_RESOLVERS=$(default_or_user DNS_RESOLVERS "External DNS resolvers coreDNS forward: csv ip:port,ip:port")
  info "DNS external resolvers: ${DNS_RESOLVERS}"
else
  info "No DNS external resolvers set apps will only have internal kubernetes DNS resolution"
fi

if [ -z "${REGISTRY_HOST_FQDN}" ]; then
  REGISTRY_HOST_FQDN=$(default_or_user REGISTRY_HOST_FQDN "must be accessible from kube/rancher nodes")
fi

if [[ -n "${REGISTRY_HOST_PORT}" ]]; then
  REGISTRY_CONNECT="${REGISTRY_HOST_FQDN}:${REGISTRY_HOST_PORT}"
else
  REGISTRY_CONNECT="${REGISTRY_HOST_FQDN}"
fi

info "REGISTRY_CONNECT is set to ${REGISTRY_CONNECT}, if you expect a specific port define with REGISTRY_HOST_PORT"

heading "Create an /etc/hosts alias for ${REGISTRY_HOST_FQDN}"

if [ -z "${REGISTRY_HOST_IP}" ]; then
   REGISTRY_HOST_IP=$(default_or_user REGISTRY_HOST_IP "Enter the IP of a docker registry" "enter, to skip creating host entry")
else
  info "REGISTRY_HOST_IP variable is set. ${REGISTRY_HOST_IP} an /etc/hosts alias will be created"
fi

heading "Rancher setup info"

info "(if the deployment is to be a rancher cluster just enter the public rancher gateway anyhow)"

if [ -z "${RANCHER_IP}" ]; then
  RANCHER_IP=$(default_or_user RANCHER_IP "IP of rancher cluster [enter skips]" "")
  RANCHER_FQDN=$(default_or_user RANCHER_FQDN "FQDN of the rancher cluster [enter skips]" "")
fi

heading "Generating Deployment"

info "${HOSTNAMES[*]}"
info "${REGISTRY_CONNECT}"
info "${RANCHER_FQDN}"

# Create a set of base templates for each host in the cluster.
for i in "${!HOSTNAMES[@]}"
do
  host_block_template=\
"- address: ${PUBLIC_IPS[$i]}
  port: \"22\"
  internal_address: ${PRIVATE_IPS[$i]}
  role:
  - controlplane
  - worker
  - etcd
  hostname_override: ${HOSTNAMES[$i]}
  user: ubuntu
  docker_socket: /var/run/docker.sock
  ssh_key: \"\"
  ssh_key_path: ~/.ssh/id_rsa
  ssh_cert: \"\"
  ssh_cert_path: \"\"
  labels: {}
  taints: []"

  HOST_BLOCKS+=("${host_block_template}")
done

#echo "${HOST_BLOCKS[*]}"

# Generate the rest of the file

CONFIG=\
"services:
  etcd:
    image: \"\"
    extra_args: {}
    extra_binds: []
    extra_env: []
    external_urls: []
    ca_cert: \"\"
    cert: \"\"
    key: \"\"
    path: \"\"
    uid: 0
    gid: 0
    snapshot: null
    retention: \"\"
    creation: \"\"
    backup_config: null
  kube-api:
    image: \"\"
    extra_args: {}
    extra_binds: []
    extra_env: []
    service_cluster_ip_range: 10.43.0.0/16
    service_node_port_range: \"\"
    pod_security_policy: false
    always_pull_images: false
    secrets_encryption_config: null
    audit_log: null
    admission_configuration: null
    event_rate_limit: null
  kube-controller:
    image: \"\"
    extra_args: {}
    extra_binds: []
    extra_env: []
    cluster_cidr: 10.42.0.0/16
    service_cluster_ip_range: 10.43.0.0/16
  scheduler:
    image: \"\"
    extra_args: {}
    extra_binds: []
    extra_env: []
  kubelet:
    image: \"\"
    extra_args: {}
    extra_binds: []
    extra_env: []
    cluster_domain: cluster.local
    infra_container_image: \"\"
    cluster_dns_server: 10.43.0.10
    fail_swap_on: false
    generate_serving_certificate: false
  kubeproxy:
    image: \"\"
    extra_args: {}
    extra_binds: []
    extra_env: []
network:
  plugin: canal
  options: {}
  mtu: 0
  node_selector: {}
authentication:
  strategy: x509
  sans: []
  webhook: null
addons: \"\"
addons_include: []
system_images:
  etcd: ${REGISTRY_CONNECT}/rancher/coreos-etcd:v3.4.3-rancher1
  alpine: ${REGISTRY_CONNECT}/rancher/rke-tools:v0.1.56
  nginx_proxy: ${REGISTRY_CONNECT}/rancher/rke-tools:v0.1.56
  cert_downloader: ${REGISTRY_CONNECT}/rancher/rke-tools:v0.1.56
  kubernetes_services_sidecar: ${REGISTRY_CONNECT}/rancher/rke-tools:v0.1.56
  kubedns: ${REGISTRY_CONNECT}/rancher/k8s-dns-kube-dns:1.15.0
  dnsmasq: ${REGISTRY_CONNECT}/rancher/k8s-dns-dnsmasq-nanny:1.15.0
  kubedns_sidecar: ${REGISTRY_CONNECT}/rancher/k8s-dns-sidecar:1.15.0
  kubedns_autoscaler: ${REGISTRY_CONNECT}/rancher/cluster-proportional-autoscaler:1.7.1
  coredns: ${REGISTRY_CONNECT}/rancher/coredns-coredns:1.6.5
  coredns_autoscaler: ${REGISTRY_CONNECT}/rancher/cluster-proportional-autoscaler:1.7.1
  nodelocal: ${REGISTRY_CONNECT}/rancher/k8s-dns-node-cache:1.15.7
  kubernetes: ${REGISTRY_CONNECT}/rancher/hyperkube:v1.17.5-rancher1
  flannel: ${REGISTRY_CONNECT}/rancher/coreos-flannel:v0.11.0-rancher1
  flannel_cni: ${REGISTRY_CONNECT}/rancher/flannel-cni:v0.3.0-rancher5
  calico_node: ${REGISTRY_CONNECT}/rancher/calico-node:v3.13.0
  calico_cni: ${REGISTRY_CONNECT}/rancher/calico-cni:v3.13.0
  calico_controllers: ${REGISTRY_CONNECT}/rancher/calico-kube-controllers:v3.13.0
  calico_ctl: ${REGISTRY_CONNECT}/rancher/calico-ctl:v2.0.0
  calico_flexvol: ${REGISTRY_CONNECT}/rancher/calico-pod2daemon-flexvol:v3.13.0
  canal_node: ${REGISTRY_CONNECT}/rancher/calico-node:v3.13.0
  canal_cni: ${REGISTRY_CONNECT}/rancher/calico-cni:v3.13.0
  canal_flannel: ${REGISTRY_CONNECT}/rancher/coreos-flannel:v0.11.0
  canal_flexvol: ${REGISTRY_CONNECT}/rancher/calico-pod2daemon-flexvol:v3.13.0
  weave_node: ${REGISTRY_CONNECT}/weaveworks/weave-kube:2.5.2
  weave_cni: ${REGISTRY_CONNECT}/weaveworks/weave-npc:2.5.2
  pod_infra_container: ${REGISTRY_CONNECT}/rancher/pause:3.1
  ingress: ${REGISTRY_CONNECT}/rancher/nginx-ingress-controller:nginx-0.25.1-rancher1
  ingress_backend: ${REGISTRY_CONNECT}/rancher/nginx-ingress-controller-defaultbackend:1.5-rancher1
  metrics_server: ${REGISTRY_CONNECT}/rancher/metrics-server:v0.3.6
  windows_pod_infra_container: ${REGISTRY_CONNECT}/rancher/kubelet-pause:v0.1.3
ssh_key_path: ~/.ssh/id_rsa
ssh_cert_path: \"\"
ssh_agent_auth: false
authorization:
  mode: rbac
  options: {}
ignore_docker_version: false
kubernetes_version: \"\"
private_registries: []
ingress:
  provider: \"\"
  options: {}
  node_selector: {}
  extra_args: {}
  dns_policy: \"\"
  extra_envs: []
  extra_volumes: []
  extra_volume_mounts: []
cluster_name: \"\"
cloud_provider:
  name: \"\"
prefix_path: \"\"
addon_job_timeout: 0
bastion_host:
  address: \"\"
  port: \"\"
  user: \"\"
  ssh_key: \"\"
  ssh_key_path: \"\"
  ssh_cert: \"\"
  ssh_cert_path: \"\"
monitoring:
  provider: \"\"
  options: {}
  node_selector: {}
restore:
  restore: false
  snapshot_name: \"\"
dns: null"

echo "Writing ${OUTFILE}"

cat > ${OUTFILE} <<EOF
nodes:
$(printf '%s\n' "${HOST_BLOCKS[@]}")
$(echo "${CONFIG}")
EOF

echo "Writing inventory.ini"
cat > inventory.ini <<EOL
[nodes]
$(printf '%s\n' "${PUBLIC_IPS[@]}")

[nodes:vars]
ansible_connection=ssh
ansible_ssh_user=ubuntu
ansible_ssh_common_args='-o StrictHostKeyChecking=no'
EOL

INVENTORY_BACKUP="${DEPLOYMENT_NAME}-inventory.ini"
echo "Creating backup of inventory ${INVENTORY_BACKUP}"
cp inventory.ini $INVENTORY_BACKUP

echo "Executing ansible playbook to copy ca-certificate bundle to cluster nodes"

mkdir -p /dev_services/_certs

cp $ROOT_CA /dev_services/_certs/ca_root.pem

ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i inventory.ini copy_ca_certificates.yml

if [[ -n "${REGISTRY_HOST_IP}" ]]; then
  echo "Updating registry in /etc/hosts"
  ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i inventory.ini -e registry_ip=$REGISTRY_HOST_IP -e registry_fqdn=$REGISTRY_HOST_FQDN registry_hosts_alias.yml
else
  echo "No $REGISTRY_HOST_IP defined skipping /etc/hosts alias, assuming you have a DNS entry for $REGISTRY_HOST_FQDN"
fi

if [ -z "${RANCHER_IP}" ]; then
  echo "skipping rancher coreDNS setup, configure coreDNS yaml template manually"
fi

ansible-playbook --connection=local -i localhost, \
  -e registry_ip=$REGISTRY_HOST_IP \
  -e registry_fqdn=$REGISTRY_HOST_FQDN \
  -e rancher_ip=$RANCHER_IP \
  -e rancher_fqdn=$RANCHER_FQDN \
  -e core_dns_resolvers=${DNS_RESOLVERS} \
  -e output_file="${DEPLOYMENT_NAME}-coredns.yml" \
  generate_coredns_configMap.yml
