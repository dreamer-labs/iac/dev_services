BEFORE RUNNING THE MIGRATION
Ensure you have created an admin account inside of the Rocketchat instance that will be restored to


1. Take snapshot of current rocketchat VM
2. SSH into VM
3. Ensure ipforwarding is enabled. `sysctl -w net.ipv4.ip_forward=1`
4. Copy over mongo images and latest rocketchat image. Load these into Docker
5. stop and rm current mongodb. `docker stop mongodb && docker rm mongodb`
6. start mongodb with mongodb:3.4
    `docker run -d -p 27017:27017 -v mongodb:/data/db --name mongodb --restart always mongo:3.4`
7. docker exec -it mongodb /bin/bash
8. mongo
9. db.adminCommand( { setFeatureCompatibilityVersion: "3.4" } )
10 exit
11. stop and rm current mongodb. `docker stop mongodb && docker rm mongodb`
12. start mongodb with mongo:3.6.8 `docker run -d -p 27017:27017 -v mongodb:/data/db --name mongodb --restart always mongo:3.6.8`
13. docker exec -it mongodb /bin/bash
14. mongo
15. db.adminCommand( { setFeatureCompatibilityVersion: "3.6" } )
16. stop and rm current mongodb. `docker stop mongodb && docker rm mongodb`
17. Start mongodb replicaset
```
docker run -d -p 27017:27017 -v mongodb:/data/db --name mongodb --restart always mongo:4.0.12 --smallfiles --oplogSize 128 --replSet rs1
```
18. docker exec -it mongodb /bin/bash
19 mongo
20. rs.initiate()
21 exit outside of container (2 exits)
docker run 
22. Stop and rm rocketchat container. `docker stop rocketchat && docker rm rocketchat`
23. Start rocketchat with new version `docker run --name rocketchat -p 80:3000 -v rocketchat:/app/uploads --env ROOT_URL=http://localhost --link mongodb:mongo -d --restart always -e "MONGO_URL=mongodb://mongo:27017/meteor" -e "MONGO_OPLOG_URL=mongodb://mongo:27017/local?replSet=rs1" registry.devservices.int/docker.io/rocketchat/rocket.chat:2.4.9

Now we need to dump this database

1. docker exec -it mongodb /bin/bash
2. mongodump
3. exit container. docker cp mongodb:/dump .
4. Copy over to staging node `scp -r dump ubuntu@10.20.5.75:~/`
4. kubectl cp dump <mongo_primary_pod>:/tmp -n rocketchat-prod
5. kubectl exec -it <mongo_pod> -n <namespace> /bin/bash
## Before running the restore make sure you have created an admin user inside of the rocketchat instance you are restoring to
6. mongorestore -u root -p <password> --nsFrom=meteor* --nsTo=rocketchat* /tmp/dump

Now continue with Rocketchat configuration (Oauth, etc)
