#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
BASE_PATH="$( cd "${SCRIPTPATH}/../deploy-tools" >/dev/null 2>&1 ; pwd -P )"
WORKING_PATH=$PWD

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

IMPORT=0

echo "This script only makes sense outside your docker-compose environment e.g. on the compose-host for now"

case $key in
    --import)
    IMPORT=1
    if [[ -f $WORKING_PATH/$2 ]];
    then
      IMPORT_FILE=$WORKING_PATH/$2
    else
      IMPORT_FILE=$2
    fi
    if ! [[ -f $IMPORT_FILE ]];
    then
      echo "$IMPORT_FILE not found"
      exit 1;
    fi
    shift # past flag
    shift # past argument
    ;;
    -h|--help)
    echo -e "Exports and imports our vault\n\tDefault: export current vault dirs as a tar.gz in CWD"
    echo -e "\n\t--import: exported tar.gz to import\n\t-h|--help: this help message"
    exit;
    shift
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ $IMPORT -eq 1 ]];
then
  if ! [[ -f $IMPORT_FILE ]];
  then
    echo "${IMPORT_FILE} does not exist";
    echo "Exiting! no actions taken"
    exit 1;
  elif ! tar -tzf $IMPORT_FILE | grep -q "file/logical";
  then
    echo "${IMPORT_FILE} doesn't look like a valid vault db"
    echo "Exiting! no actions taken"
    exit 1;
  fi
  (
    echo "Bringing docker-compose down to close data backend"
    cd ${BASE_PATH}/deploy-tools;
    docker-compose down
    rm -r volumes/file volumes/config volumes/nawt >/dev/null 2>&1
    tar -xzf $IMPORT_FILE
  )
else
  (
    cd ${BASE_PATH}/deploy-tools;
    echo "Bringing docker-compose down to close data backend"
    docker-compose down
    find -regex "\./volumes/\(config\|file\|nawt\)/.*" -regextype posix-egrep \
      | tar -czf "${WORKING_PATH}/vault-$(date +"%d-%m-%Y").tar.gz" -T -;
  )
  echo "Exported vault data to ${WORKING_PATH}/vault-$(date +"%d-%m-%Y").tar.gz"
fi
