#!/usr/bin/env bash

vault_logfile=/persist/last-dev.txt


grep "Unseal Key" $vault_logfile | while read -r key_line;
do
  vault operator unseal $(echo -n "${key_line}" | grep -o ": .*" | tr -d ': ')
done

grep "Initial Root Token" $vault_logfile
