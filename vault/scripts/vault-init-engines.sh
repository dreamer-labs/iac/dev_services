
vault secrets enable -version=2 -description="dev environ secrets" -path="dev/secrets" kv
vault secrets enable -version=2 -description="preview environ secrets" -path="preview/secrets" kv

vault secrets list
