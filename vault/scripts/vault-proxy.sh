#!/bin/bash

echo -n "socat will forward 8200 from the vault sidecar to 8200 of this container this "
echo "will allow access to your vault API from outside this container"
socat tcp-l:8200,fork,reuseaddr tcp:vault:8200
