#!/bin/bash
vault_logfile=/persist/last-dev.txt

vault operator init -key-shares=3 -key-threshold=3 | tee -a /persist/vault-creation.txt | tee $vault_logfile

echo "You can chown volumes/* to your user/group outside the container to remote root perms"
